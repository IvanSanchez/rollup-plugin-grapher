
var path = require('path');
var fs = require('fs');
var cwd = process.cwd();

module.exports = function rollupGrapher(opts) {

	if (!opts) { opts = {}; }
	if (!opts.dest) { opts.dest = 'rollup-grapher.html' }

	return {

		ongenerate: function(opts, bundle) { return this._onEnd(opts, bundle) },
		onwrite:    function(opts, bundle) { return this._onEnd(opts, bundle) },

		_onEnd: function(generated) {
			var i;
			var modules = generated.bundle.modules;

			var fullEntryPoint = path.join(cwd, generated.entry);
			var entryDir = path.dirname(fullEntryPoint);

			function mkRelativePath(moduleId) {
				return path.relative(entryDir, moduleId);
			}

			var nodes = [];	// module IDs
			var links = [];	// elements: {source: moduleid, target: moduleid}

			for (i in modules) {
				var module = modules[i];

				var relativeModuleId = mkRelativePath(modules[i].id);
				nodes.push(relativeModuleId);

				for (var j in module.dependencies) {
					var relativeDepencencyId = mkRelativePath(module.dependencies[j]);
					if (opts.verbose) {
						console.log('Dependency: ', relativeModuleId, '→', relativeDepencencyId);
					}
					links.push({source: relativeModuleId, target: relativeDepencencyId});
				}
			}

			// Placeholders for d3-friendly data
			var nodeNames = [];
			var nodeRawNames= [];
			var indexedLinks = [];

			var handlebars = require('handlebars');
			var template = handlebars.compile( fs.readFileSync(
				path.join(__dirname, 'rollup-grapher.hbs')).toString()
			);

			// Format data so D3 likes it
			var i, nodeNames = [], indexedLinks = [], nodeRawNames = [];
			for (i in nodes) {
				nodeNames.push({id: nodes[i] /*, group: nodes[i].group*/ });
				nodeRawNames.push(nodes[i]);
			}

			for (i in links) {
				var sourceId = nodeRawNames.indexOf(links[i].source);
				var targetId = nodeRawNames.indexOf(links[i].target);

				if (sourceId !== -1 && targetId !== -1) {
					indexedLinks.push({
						source: links[i].source,
						target: links[i].target,
						value: 1
					});
				}
			}

			// Output the html file including the data
			fs.writeFileSync( opts.dest, template({
				links: JSON.stringify(indexedLinks),
				nodes: JSON.stringify(nodeNames)
			}));
		}
	};
}

