# rollup-plugin-grapher

[Rollup](http://www.rollupjs.org) plugin to create an interactive module graph (powered by [D3](https://github.com/d3/d3))


![](screenshot.png)

## Installation

```bash
yarn add --dev rollup-plugin-grapher
```
or
```bash
npm install --save-dev rollup-plugin-grapher
```


## Usage

As with any other Rollup plugin, just add it to the `plugins` option in your RollupJS config.

```js
// In rollup.config.js
import rollupGrapher from 'rollup-plugin-grapher'

export default {
	entry: 'src/index.js',
	dest: 'dist/my-lib.js',
	plugins: [
		rollupGrapher()
	]
};
```

That will create a file named `rollup-grapher.html` when you generate the RollupJS bundle.
If you want to change the name or location of this file, specify a `dest` option to the plugin.

This plugin can also output all the module dependencies in the console by specifying
a boolean `verbose` option.

e.g.:

```js
// In rollup.config.js
import rollupGrapher from 'rollup-plugin-grapher'

export default {
	entry: 'src/index.js',
	dest: 'dist/my-lib.js',
	plugins: [
		rollupGrapher({
			dest: 'artifacts/module-dependency-graph.html',
			verbose: true
		})
	]
};
```

## License

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
